## Vending Machine

This is a simple vending machine developed using the spring boot. It allows the user to insert a selected coin, then using the keypad, enter a combination to select an item, dispense the item and dispense change.
 

### Gradle Docker:

Gradle Docker plugin has been included to build this as a docker image. Run it locally using command below : 

```bash
$ docker run -p 8080:8080 -t com.cyberpro.lrm/vendingmachine-docker
```