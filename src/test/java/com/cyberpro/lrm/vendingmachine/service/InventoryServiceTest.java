package com.cyberpro.lrm.vendingmachine.service;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.Before;
import org.junit.Test;

import com.cyberpro.lrm.vendingmachine.exception.ProductSoldOutException;
import com.cyberpro.lrm.vendingmachine.pojo.InventorySlot;
import com.cyberpro.lrm.vendingmachine.pojo.Product;
import com.cyberpro.lrm.vendingmachine.service.impl.InventoryServiceImpl;

public class InventoryServiceTest {

	private InventoryService service;

	@Before
	public void setup() throws Exception {

		service = new InventoryServiceImpl();
	}

	@Test
	public void getSlotById_whenSlotProductEmpty_shouldReturnCountZero() throws Exception {
		InventorySlot slot = new InventorySlot("A", new Product("123", "Coke", 10.00), 0);
		service.addSlot(slot);

		assertThat(service.getSlotById("A").getCount(), is(0));
	}

	@Test
	public void getSlotByIdGetProducts_whenSlotHasOneProduct_shouldReturnSizeOne() throws Exception {
		Product product = new Product("123", "Coke", 10.00);

		InventorySlot slot = new InventorySlot("A", product, 1);
		service.addSlot(slot);

		assertThat(service.getSlotById("A").getCount(), is(1));
	}

	@Test
	public void getSlotByIdGetProducts_whenSlotHasOneProductAndOneIsAdded_shouldReturnSizeTwo() throws Exception {
		Product product = new Product("123", "Coke", 10.00);
		InventorySlot slot = new InventorySlot("A", product, 0);
		slot.setCount(2);
		service.addSlot(slot);

		assertThat(service.getSlotById("A").getCount(), is(2));
	}

	@Test(expected = ProductSoldOutException.class)
	public void getSlotByIdDispenseNext_whenSlotEmpty_shouldThrowProductSoldOutException() throws Exception {
		Product product = new Product("123", "Coke", 10.00);
		InventorySlot slot = new InventorySlot("A", product, 0);
		service.addSlot(slot);

		service.getSlotById("A").dispenseNext();
	}

	@Test
	public void getSlotByIdDispenseNext_whenProductCoke_shouldReturnCoke() throws Exception {
		Product product = new Product("123", "Coke", 10.00);

		InventorySlot slot = new InventorySlot("A", product, 1);
		service.addSlot(slot);

		Product actualProduct = service.getSlotById("A").dispenseNext();

		assertThat(actualProduct, is(product));
	}

	@Test
	public void getSlotByIdDispenseNext_whenProductListHasTwoProductCokesAndOneIsDispensed_shouldReturnSizeOne()
			throws Exception {
		Product product = new Product("123", "Coke", 10.00);

		InventorySlot slot = new InventorySlot("A", product, 2);
		service.addSlot(slot);

		service.getSlotById("A").dispenseNext();

		assertThat(service.getSlotById("A").getCount(), is(1));
	}

}
