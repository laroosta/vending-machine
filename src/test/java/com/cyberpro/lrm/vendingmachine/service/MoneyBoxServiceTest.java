package com.cyberpro.lrm.vendingmachine.service;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.hasSize;

import org.junit.Before;
import org.junit.Test;

import com.cyberpro.lrm.vendingmachine.pojo.BankNote;
import com.cyberpro.lrm.vendingmachine.pojo.BankNote.NoteType;
import com.cyberpro.lrm.vendingmachine.pojo.Coin;
import com.cyberpro.lrm.vendingmachine.pojo.Coin.CoinType;
import com.cyberpro.lrm.vendingmachine.pojo.Money;
import com.cyberpro.lrm.vendingmachine.service.MoneyBoxService;
import com.cyberpro.lrm.vendingmachine.service.impl.MoneyBoxServiceImpl;

/**
 * @author lmichelson
 *
 */
public class MoneyBoxServiceTest {

	MoneyBoxService service;

	@Before
	public void setup() throws Exception {
		service = new MoneyBoxServiceImpl();
	}

	@Test
	public void getTotal_whenTenNote_totalAmountShouldBeTen() throws Exception {
		service.deposit(new BankNote(NoteType.TEN));

		assertThat(service.getTotal(), is(10.0));
	}

	@Test
	public void getTotal_whenOneTenNoteAndOneTwenty_totalAmountShouldBeThirty() throws Exception {
		service.deposit(new BankNote(NoteType.TEN));
		service.deposit(new BankNote(NoteType.TWENTY));

		assertThat(service.getTotal(), is(30.0));
	}

	@Test
	public void getMoneyList_whenOneTenNoteAndOneTwenty_moneyListShouldHasSizeOf2() throws Exception {
		service.deposit(new BankNote(NoteType.TEN));
		service.deposit(new BankNote(NoteType.TWENTY));

		assertThat(service.getMoneyList(), hasSize(2));
	}

	@Test
	public void getTotal_whenOneTenNoteAndOneFiftyCents_totalAmountShouldBeTenPoint50() throws Exception {
		service.deposit(new BankNote(NoteType.TEN));
		service.deposit(new Coin(CoinType.FIFTYCENTS));

		assertThat(service.getTotal(), is(10.50));
	}

	@Test
	public void getMoneyList_whenOneTenNoteAndOneTwenty_moneyListShouldReturnOneTenNoteAndOneTwenty() throws Exception {
		Money tenNote = new BankNote(NoteType.TEN);
		Money twentyNote = new BankNote(NoteType.TWENTY);

		service.deposit(tenNote);
		service.deposit(twentyNote);

		assertThat(service.getMoneyList(), hasItems(tenNote, twentyNote));
	}

	@Test
	public void getAllocationList_whenOneTenNoteAndOneTwenty_allocationListShouldReturnOnNoteTypeWithValueOf10()
			throws Exception {
		Money tenNote = new BankNote(NoteType.TEN);
		Money twentyNote = new BankNote(NoteType.TWENTY);

		service.deposit(tenNote);
		service.deposit(twentyNote);

		assertThat(service.getMoneyList(), hasItems(tenNote, twentyNote));
	}

}
