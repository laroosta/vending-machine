package com.cyberpro.lrm.vendingmachine.service;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.hasValue;
import static org.hamcrest.Matchers.is;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;

import com.cyberpro.lrm.vendingmachine.exception.NotEnoughChangeException;
import com.cyberpro.lrm.vendingmachine.pojo.Allocation;
import com.cyberpro.lrm.vendingmachine.service.impl.MoneyDispenserServiceImpl;

/**
 * @author lmichelson
 *
 */
public class MoneyDispenserServiceTest {

	private MoneyDispenserService service;
	private List<Allocation> allocations;

	@Before
	public void setup() throws Exception {
		Map<Double, Integer> dummyData = new HashMap<>();
		dummyData.put(5.00, 10);
		dummyData.put(2.00, 10);
		dummyData.put(1.00, 10);
		dummyData.put(0.50, 10);

		allocations = dummyData.entrySet().stream().map(d -> new Allocation(d.getKey(), d.getValue()))
				.collect(Collectors.toList());

		service = new MoneyDispenserServiceImpl();
	}

	@Test
	public void calculateDenominations_whenAmountIsZero_shouldReturnSizeOfZero() throws Exception {
		double amount = 0;

		List<Allocation> denominations = service.calculateDenominations(allocations, amount);

		assertThat(denominations, hasSize(0));
	}

	@Test
	public void calculateDenominations_whenAmountFive_shouldOnlyHaveOneDenomination() throws Exception {
		double amount = 5;

		List<Allocation> denominations = service.calculateDenominations(allocations, amount);

		assertThat(denominations, hasSize(1));
	}

	@Test
	public void calculateDenominations_whenAmountFive_shouldOnlyHaveOneDenominationWithValueOfFive() throws Exception {
		double amount = 5;

		List<Allocation> denominations = service.calculateDenominations(allocations, amount);

		assertThat(denominations.get(0).getCount(), is(1));
	}

	@Test
	public void calculateDenominations_whenAmountTen_shouldHaveTwoDenominationWithValueOfFive() throws Exception {
		double amount = 10;

		List<Allocation> denominations = service.calculateDenominations(allocations, amount);

		assertThat(denominations.get(0).getCount(), is(2));
	}

	@SuppressWarnings("unchecked")
	@Test
	public void calculateDenominations_whenAmountEightFifty_shouldHaveOneOfEachDenominations() throws Exception {
		double amount = 8.50;

		List<Allocation> denominations = service.calculateDenominations(allocations, amount);
		Map<Double, Integer> demonimationMap = denominations.stream()
				.collect(Collectors.toMap(Allocation::getValue, Allocation::getCount));

		assertThat(demonimationMap, allOf(hasValue(is(1))));
	}

	@Test
	public void calculateDenominations_whenAmountSixtyEightPointFive_shouldDenominateAllFivesAndRemainderFromTwosAndFiftyCents()
			throws Exception {
		double amount = 68.50;

		Map<Double, Integer> dummyData = new HashMap<>();
		dummyData.put(5.00, 10);
		dummyData.put(2.00, 9);
		dummyData.put(0.50, 1);

		List<Allocation> denominations = service.calculateDenominations(allocations, amount);
		Map<Double, Integer> demonimationMap = denominations.stream()
				.collect(Collectors.toMap(Allocation::getValue, Allocation::getCount));

		assertThat(demonimationMap.entrySet(), Matchers.everyItem(Matchers.isIn(dummyData.entrySet())));
	}

	@Test
	public void calculateDenominations_whenAmountFiveAndNoFivesAvailable_shouldDenominateToTwoTwosAndOneOne()
			throws Exception {
		double amount = 5.00;
		Map<Double, Integer> dummyData = new HashMap<>();
		dummyData.put(5.00, 0);
		dummyData.put(2.00, 10);
		dummyData.put(1.00, 10);
		dummyData.put(0.50, 10);

		allocations = dummyData.entrySet().stream().map(d -> new Allocation(d.getKey(), d.getValue()))
				.collect(Collectors.toList());

		Map<Double, Integer> expectedMap = new HashMap<>();
		expectedMap.put(5.00, 0);
		expectedMap.put(2.00, 2);
		expectedMap.put(1.00, 1);
		expectedMap.put(0.50, 0);

		List<Allocation> denominations = service.calculateDenominations(allocations, amount);
		Map<Double, Integer> demonimationMap = denominations.stream()
				.collect(Collectors.toMap(Allocation::getValue, Allocation::getCount));

		assertThat(demonimationMap.entrySet(), Matchers.everyItem(Matchers.isIn(expectedMap.entrySet())));
	}

	@Test(expected = NotEnoughChangeException.class)
	public void calculateDenominations_whenAmountNineHundred_shouldThrowNotEnoughChangeException() throws Exception {
		double amount = 900;

		service.calculateDenominations(allocations, amount);
	}

	@Test(expected = NotEnoughChangeException.class)
	public void calculateDenominations_whenNoSolution_shouldThrowNotEnoughChangeException() throws Exception {
		double amount = 4.00;
		Map<Double, Integer> dummyData = new HashMap<>();
		dummyData.put(5.00, 1);
		dummyData.put(2.00, 1);
		dummyData.put(1.00, 00);
		dummyData.put(0.50, 00);

		allocations = dummyData.entrySet().stream().map(d -> new Allocation(d.getKey(), d.getValue()))
				.collect(Collectors.toList());

		service.calculateDenominations(allocations, amount);

	}

}
