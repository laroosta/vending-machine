package com.cyberpro.lrm.vendingmachine.service;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.cyberpro.lrm.vendingmachine.pojo.Allocation;
import com.cyberpro.lrm.vendingmachine.pojo.BankNote;
import com.cyberpro.lrm.vendingmachine.pojo.BankNote.NoteType;
import com.cyberpro.lrm.vendingmachine.pojo.Coin;
import com.cyberpro.lrm.vendingmachine.pojo.Coin.CoinType;
import com.cyberpro.lrm.vendingmachine.pojo.Money;
import com.cyberpro.lrm.vendingmachine.service.AllocationService;
import com.cyberpro.lrm.vendingmachine.service.impl.AllocationServiceImpl;

public class AllocationServiceTest {

	private AllocationService service;
	private List<Money> moneyList;

	@Before
	public void setup() throws Exception {

		service = new AllocationServiceImpl();
	}

	@Test
	public void getAllocation_whenEmpty_shouldReturnSizeOf0() throws Exception {
		moneyList = new ArrayList<Money>();

		assertThat(service.getAllocation(moneyList).size(), is(0));
	}

	@Test
	public void getAllocation_whenTenAndTwenty_shouldReturnSizeOf2() throws Exception {
		moneyList = new ArrayList<Money>();
		moneyList.add(new BankNote(NoteType.TEN));
		moneyList.add(new BankNote(NoteType.TWENTY));

		assertThat(service.getAllocation(moneyList).size(), is(2));
	}

	@Test
	public void getAllocation_whenTenAndTen_shouldReturnSizeOf2() throws Exception {
		moneyList = new ArrayList<Money>();
		moneyList.add(new BankNote(NoteType.TEN));
		moneyList.add(new Coin(CoinType.FIFTYCENTS));

		assertThat(service.getAllocation(moneyList).size(), is(2));
	}

	@Test
	public void getAllocation_whenTenAndTen_shouldReturnMapWithTenCount2() throws Exception {
		moneyList = new ArrayList<Money>();
		moneyList.add(new BankNote(NoteType.TEN));
		moneyList.add(new BankNote(NoteType.TEN));

		Allocation allocation = service.getAllocation(moneyList).get(0);

		assertThat(allocation.getCount(), is(2));
	}

}
