package com.cyberpro.lrm.vendingmachine;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VendingmachineApplication {

	private static final Logger log = LoggerFactory.getLogger(VendingmachineApplication.class);

	public static void main(String[] args) {

		log.info("+---------------------------------------------+");
		log.info("+ Starting up Vending Machine");
		log.info("+---------------------------------------------+");

		SpringApplication.run(VendingmachineApplication.class, args);
	}

}
