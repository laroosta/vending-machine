package com.cyberpro.lrm.vendingmachine.ui.event;

import java.util.List;

import org.springframework.context.ApplicationEvent;

import com.cyberpro.lrm.vendingmachine.pojo.Allocation;

public class DispenseMoneyEvent extends ApplicationEvent {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<Allocation> allocation;

	public DispenseMoneyEvent(Object source, List<Allocation> allocation) {
		super(source);
		this.allocation = allocation;
	}

	public List<Allocation> getAllocation() {
		return allocation;
	}

}
