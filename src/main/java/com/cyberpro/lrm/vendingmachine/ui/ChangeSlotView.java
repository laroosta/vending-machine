package com.cyberpro.lrm.vendingmachine.ui;

import javax.annotation.PostConstruct;

import org.springframework.context.ApplicationListener;

import com.cyberpro.lrm.vendingmachine.pojo.Allocation;
import com.cyberpro.lrm.vendingmachine.ui.event.DispenseMoneyEvent;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;

@UIScope
@SpringComponent
public class ChangeSlotView extends VerticalLayout implements ApplicationListener<DispenseMoneyEvent> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Grid<Allocation> grid;

	public ChangeSlotView() {
		setWidth("100%");
		getStyle().set("border", "1px solid #9E9E9E");
		this.grid = new Grid<>(Allocation.class);
		grid.setHeightByRows(true);
	}

	@PostConstruct
	public void init() throws Exception {
		add(new Label("Change"), grid);
	}

	@Override
	public void onApplicationEvent(DispenseMoneyEvent event) {
		if (event.getAllocation() != null) {
			grid.setItems(event.getAllocation());
		}
	}

}
