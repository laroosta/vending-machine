package com.cyberpro.lrm.vendingmachine.ui.event;

import org.springframework.context.ApplicationEvent;

public class DisplayChangeEvent extends ApplicationEvent {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String topDisplay;
	private String bottomDisplay;

	public DisplayChangeEvent(Object source, String topDisplay, String bottomDisplay) {
		super(source);
		this.topDisplay = topDisplay;
		this.bottomDisplay = bottomDisplay;
	}

	public String getTopDisplay() {
		return topDisplay;
	}

	public String getBottomDisplay() {
		return bottomDisplay;
	}

}
