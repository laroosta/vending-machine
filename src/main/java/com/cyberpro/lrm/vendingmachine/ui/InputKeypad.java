package com.cyberpro.lrm.vendingmachine.ui;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;

import com.cyberpro.lrm.vendingmachine.ui.event.DisplayChangeEvent;
import com.cyberpro.lrm.vendingmachine.ui.event.SystemEvent;
import com.cyberpro.lrm.vendingmachine.ui.event.SystemEvent.SystemEventType;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;

@UIScope
@SpringComponent
public class InputKeypad extends VerticalLayout {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public final HorizontalLayout topRow;
	public final HorizontalLayout bottomRow;

	public String entry = new String();

	private ApplicationEventPublisher applicationEventPublisher;

	@Autowired
	public void setApplicationEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
		this.applicationEventPublisher = applicationEventPublisher;
	}

	public InputKeypad() {
		setWidth("100%");
		getStyle().set("border", "1px solid #9E9E9E");
		this.topRow = new HorizontalLayout();
		this.bottomRow = new HorizontalLayout();

		add(new Label("Keypad"), topRow, bottomRow);
	}

	@PostConstruct
	public void init() {
		String[] keys = new String[2];
		keys[0] = "A";
		keys[1] = "B";

		for (String key : keys) {
			Button keyButton = new Button(key);
			keyButton.addClickListener(e -> {
				onEntryClick(key);
			});
			topRow.add(keyButton);
		}

		Button clearKey = new Button("Clear");
		clearKey.addClickListener(e -> {
			entry = "";
			SystemEvent systemEvent = new SystemEvent(this, SystemEventType.CLEAR_INPUT, "Clear Button");
			applicationEventPublisher.publishEvent(systemEvent);
		});

		bottomRow.add(clearKey);
	}

	private void onEntryClick(String key) {
		entry += key;

		String message = "Selection : " + entry;

		if (entry.length() > 1) {
			SystemEvent systemEvent = new SystemEvent(this, SystemEventType.DISPENSE_ITEM, message, entry);
			applicationEventPublisher.publishEvent(systemEvent);

			entry = "";
		} else {
			DisplayChangeEvent displayChangeEvent = new DisplayChangeEvent(this, message, null);
			applicationEventPublisher.publishEvent(displayChangeEvent);
		}
	}
}
