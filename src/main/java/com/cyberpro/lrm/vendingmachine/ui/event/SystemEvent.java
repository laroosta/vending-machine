package com.cyberpro.lrm.vendingmachine.ui.event;

import org.springframework.context.ApplicationEvent;

/**
 * @author lmichelson
 *
 */
public class SystemEvent extends ApplicationEvent {

	private SystemEventType type;

	public enum SystemEventType {
		RESET_SYSTEM, CLEAR_INPUT, INSERT_MONEY, RETURN_MONEY, DISPENSE_ITEM;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String message;
	private Object[] object;

	public SystemEvent(Object source, SystemEventType type) {
		this(source, type, null);
	}

	public SystemEvent(Object source, SystemEventType type, String message) {
		super(source);
		this.type = type;
		this.message = message;
	}

	public SystemEvent(Object source, SystemEventType type, String message, Object... object) {
		super(source);
		this.type = type;
		this.message = message;
		this.object = object;
	}

	public SystemEventType getType() {
		return type;
	}

	public String getMessage() {
		return message;
	}

	public Object[] getObject() {
		return object;
	}

}
