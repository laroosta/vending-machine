package com.cyberpro.lrm.vendingmachine.ui;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;

import com.cyberpro.lrm.vendingmachine.pojo.InventorySlot;
import com.cyberpro.lrm.vendingmachine.service.InventoryService;
import com.cyberpro.lrm.vendingmachine.ui.event.DispenseProductEvent;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.Grid.SelectionMode;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;

@UIScope
@SpringComponent
public class LeftLayout extends VerticalLayout implements ApplicationListener<DispenseProductEvent> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Grid<InventorySlot> grid;

	@Autowired
	InventoryService inventoryService;

	public LeftLayout() {
		setWidth("100%");
		getStyle().set("border", "1px solid #9E9E9E");

		this.grid = new Grid<>(InventorySlot.class);
	}

	@PostConstruct
	public void init() throws Exception {
		grid.setColumns("slotId", "product.name", "product.sellingPrice", "count");

		grid.setItems(inventoryService.getAll());
		grid.setSelectionMode(SelectionMode.NONE);

		add(grid);

	}

	@Override
	public void onApplicationEvent(DispenseProductEvent event) {
		grid.setItems(inventoryService.getAll());
	}
}
