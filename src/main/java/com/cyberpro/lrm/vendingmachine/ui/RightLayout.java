package com.cyberpro.lrm.vendingmachine.ui;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;

@UIScope
@SpringComponent
public class RightLayout extends VerticalLayout {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Autowired
	CoinSlotView coinSlotView;

	@Autowired
	ChangeSlotView changeSlotView;

	@Autowired
	DisplayView displayView;

	@Autowired
	InputKeypad inputKeypad;

	public RightLayout() {
		setWidth("100%");
		getStyle().set("border", "1px solid #9E9E9E");
	}

	@PostConstruct
	public void init() throws Exception {
		add(coinSlotView, displayView, inputKeypad, changeSlotView);
	}

}
