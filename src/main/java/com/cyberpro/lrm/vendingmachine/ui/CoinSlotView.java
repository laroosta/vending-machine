package com.cyberpro.lrm.vendingmachine.ui;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationEventPublisher;

import com.cyberpro.lrm.vendingmachine.pojo.Coin;
import com.cyberpro.lrm.vendingmachine.pojo.Coin.CoinType;
import com.cyberpro.lrm.vendingmachine.pojo.Money;
import com.cyberpro.lrm.vendingmachine.service.MoneyBoxService;
import com.cyberpro.lrm.vendingmachine.service.MoneySlotService;
import com.cyberpro.lrm.vendingmachine.ui.event.SystemEvent;
import com.cyberpro.lrm.vendingmachine.ui.event.SystemEvent.SystemEventType;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;

@UIScope
@SpringComponent
public class CoinSlotView extends VerticalLayout {

	private static final Logger log = LoggerFactory.getLogger(CoinSlotView.class);
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final Button insertCoin = new Button("Insert");
	private final Button returnCoins = new Button("Return");
	private ApplicationEventPublisher applicationEventPublisher;
	private MoneySlotService coinSlotService;
	private MoneyBoxService moneyBoxService;

	@Autowired
	public void setApplicationEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
		this.applicationEventPublisher = applicationEventPublisher;
	}

	@Autowired
	@Qualifier("coinSlot")
	public void setMoneySlotService(MoneySlotService coinSlotService) {
		this.coinSlotService = coinSlotService;
	}

	@Autowired
	public void setMoneyBoxService(MoneyBoxService moneyBoxService) {
		this.moneyBoxService = moneyBoxService;
	}

	public CoinSlotView() {
		setWidth("100%");
		getStyle().set("border", "1px solid #9E9E9E");
	}

	@PostConstruct
	public void init() {
		ComboBox<Money> coinSelect = buildCoinSelection();
		insertCoin.addClickListener(e -> {
			if (coinSelect.getValue() != null) {
				onInsertClicked(coinSelect);
			}
		});

		returnCoins.addClickListener(e -> {
			onReturnClicked();
		});

		HorizontalLayout buttonLayout = new HorizontalLayout();
		buttonLayout.add(insertCoin, returnCoins);
		add(new Label("Cloin Slot"), coinSelect, buttonLayout);
	}

	private ComboBox<Money> buildCoinSelection() {
		ComboBox<Money> coinSelect = new ComboBox<>();
		coinSelect.setItemLabelGenerator(m -> "" + m.getValue());
		coinSelect.setAllowCustomValue(false);

		List<Money> coinList = new ArrayList<>();
		coinList.add(new Coin(CoinType.FIFTYCENTS));
		coinList.add(new Coin(CoinType.ONE));
		coinList.add(new Coin(CoinType.TWO));
		coinList.add(new Coin(CoinType.FIVE));
		coinSelect.setItems(coinList);
		return coinSelect;
	}

	private void onReturnClicked() {
		log.debug("Returning Coins");

		SystemEvent systemEvent = new SystemEvent(this, SystemEventType.RETURN_MONEY);

		applicationEventPublisher.publishEvent(systemEvent);
	}

	private void onInsertClicked(ComboBox<Money> coinSelect) {
		log.debug("Inserting Coin");

		Money money = coinSelect.getValue();
		coinSlotService.insert(money);
		moneyBoxService.deposit(money);

		String message = "Inserted : " + money.getValue();
		SystemEvent systemEvent = new SystemEvent(this, SystemEventType.INSERT_MONEY, message);

		applicationEventPublisher.publishEvent(systemEvent);
	}

}
