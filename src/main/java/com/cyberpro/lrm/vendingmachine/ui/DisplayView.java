package com.cyberpro.lrm.vendingmachine.ui;

import org.springframework.context.ApplicationListener;

import com.cyberpro.lrm.vendingmachine.ui.event.DisplayChangeEvent;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;

@UIScope
@SpringComponent
public class DisplayView extends VerticalLayout implements ApplicationListener<DisplayChangeEvent> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	final Label topLabel = new Label();
	final Label bottomLabel = new Label();

	public DisplayView() {
		setWidth("100%");
		getStyle().set("border", "1px solid #9E9E9E");
		getStyle().set("background-color", "#666666");

		topLabel.getStyle().set("color", "#FFF");
		bottomLabel.getStyle().set("color", "#FFF");
		add(topLabel, bottomLabel);
	}

	@Override
	public void onApplicationEvent(DisplayChangeEvent event) {
		if (event.getTopDisplay() != null) {
			topLabel.setText(event.getTopDisplay());
		}

		if (event.getBottomDisplay() != null) {
			bottomLabel.setText(event.getBottomDisplay());
		}
	}

}
