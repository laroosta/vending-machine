package com.cyberpro.lrm.vendingmachine.ui.event;

import org.springframework.context.ApplicationEvent;

import com.cyberpro.lrm.vendingmachine.pojo.InventorySlot;

public class DispenseProductEvent extends ApplicationEvent {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public DispenseProductEvent(Object source, InventorySlot inventory) {
		super(source);
	}

}
