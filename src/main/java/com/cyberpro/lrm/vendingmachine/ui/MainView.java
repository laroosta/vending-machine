package com.cyberpro.lrm.vendingmachine.ui;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;

import com.cyberpro.lrm.vendingmachine.ui.event.SystemEvent;
import com.cyberpro.lrm.vendingmachine.ui.event.SystemEvent.SystemEventType;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.router.Route;

@Route
public class MainView extends HorizontalLayout {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Autowired
	RightLayout rightLayout;

	@Autowired
	LeftLayout leftLayout;

	@Autowired
	private ApplicationEventPublisher applicationEventPublisher;

	public MainView() {
		setWidth("100%");
		getStyle().set("border", "1px solid #9E9E9E");
	}

	@PostConstruct
	public void init() throws Exception {
		add(leftLayout, rightLayout);
		applicationEventPublisher.publishEvent(new SystemEvent(this, SystemEventType.RESET_SYSTEM));
	}

}
