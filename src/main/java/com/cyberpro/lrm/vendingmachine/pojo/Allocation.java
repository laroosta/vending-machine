package com.cyberpro.lrm.vendingmachine.pojo;

/**
 * @author lmichelson
 *
 */
public class Allocation {

	private double value;
	private int count;

	public Allocation(double value, int count) {
		super();
		this.value = value;
		this.count = count;
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

}
