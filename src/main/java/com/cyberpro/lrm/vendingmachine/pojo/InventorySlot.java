package com.cyberpro.lrm.vendingmachine.pojo;

import com.cyberpro.lrm.vendingmachine.exception.ProductSoldOutException;

public class InventorySlot {

	private String slotId;
	private Product product;
	private int count;

	public InventorySlot(String slotId, Product product, int count) {
		super();
		this.slotId = slotId;
		this.product = product;
		this.count = count;
	}

	public String getSlotId() {
		return slotId;
	}

	public void setSlotId(String slotId) {
		this.slotId = slotId;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public void addCount() {
		this.count++;
	}

	public Product dispenseNext() throws ProductSoldOutException {

		if (this.count == 0) {
			throw new ProductSoldOutException();
		}

		this.count--;

		return this.product;
	}

}
