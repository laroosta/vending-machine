package com.cyberpro.lrm.vendingmachine.pojo;

/**
 * This class is used for bank notes
 * 
 * @author lmichelson
 *
 */
public class BankNote extends Money {

	private NoteType type;

	public enum NoteType {
		TEN(10.00), TWENTY(20.00), FIFTY(50.00), HUNDRED(100.00), TWOHUNDRED(200.00);

		private final double value;

		NoteType(double value) {
			this.value = value;
		}

		public double getValue() {
			return this.value;
		}
	}

	/**
	 * Constructs a bank note with the specified type
	 * 
	 * @param type
	 * @see NoteType
	 */
	public BankNote(NoteType type) {
		super(type.getValue());
		this.type = type;
	}

	/**
	 * Returns the type of bank note
	 * 
	 * @return the type of bank note
	 */
	public NoteType getType() {
		return type;
	}

}
