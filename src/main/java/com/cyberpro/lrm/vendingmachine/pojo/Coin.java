package com.cyberpro.lrm.vendingmachine.pojo;

/**
 * This class is used for coins
 * 
 * @author lmichelson
 *
 */
public class Coin extends Money {

	private CoinType type;

	public enum CoinType {
		FIFTYCENTS(0.50), ONE(1.00), TWO(2.00), FIVE(5.00);

		private final double value;

		CoinType(double value) {
			this.value = value;
		}

		public double getValue() {
			return this.value;
		}
	}

	/**
	 * Constructs a coin with the specified type
	 * 
	 * @param type
	 * @see CoinType
	 */
	public Coin(CoinType type) {
		super(type.getValue());
		this.type = type;
	}

	public CoinType getType() {
		return type;
	}

}
