package com.cyberpro.lrm.vendingmachine.pojo;

/**
 * @author lmichelson
 *
 */
public class Product {

	private String skuCode;
	private String name;
	private double sellingPrice;

	public Product(String skuCode, String name, double sellingPrice) {
		super();
		this.skuCode = skuCode;
		this.name = name;
		this.sellingPrice = sellingPrice;
	}

	public String getSkuCode() {
		return skuCode;
	}

	public void setSkuCode(String skuCode) {
		this.skuCode = skuCode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getSellingPrice() {
		return sellingPrice;
	}

	public void setSellingPrice(double sellingPrice) {
		this.sellingPrice = sellingPrice;
	}

}
