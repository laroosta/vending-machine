package com.cyberpro.lrm.vendingmachine.pojo;

/**
 * This class is the abstract for the money allowed in this vending machine
 * 
 * @author lmichelson
 * @see Coin
 * @see BankNote
 */
public abstract class Money {

	private double value;

	protected Money(double value) {
		super();
		this.value = value;
	}

	/**
	 * Returns the value of the money
	 * 
	 * @return
	 */
	public double getValue() {
		return value;
	}

}
