package com.cyberpro.lrm.vendingmachine.exception;

/**
 * The NoChangeException class is used when there is an error dispensing change.
 * 
 * @author lmichelson
 *
 */
public class NotEnoughChangeException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2486850694123994706L;

}
