package com.cyberpro.lrm.vendingmachine.exception;

/**
 * @author lmichelson
 *
 */
public class ProductSoldOutException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4632283709810555606L;

}
