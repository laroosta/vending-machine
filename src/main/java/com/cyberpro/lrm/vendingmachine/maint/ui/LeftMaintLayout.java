package com.cyberpro.lrm.vendingmachine.maint.ui;

import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationListener;

import com.cyberpro.lrm.vendingmachine.exception.ProductNotFoundException;
import com.cyberpro.lrm.vendingmachine.exception.ProductSoldOutException;
import com.cyberpro.lrm.vendingmachine.pojo.InventorySlot;
import com.cyberpro.lrm.vendingmachine.service.InventoryService;
import com.cyberpro.lrm.vendingmachine.ui.event.DispenseProductEvent;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.Grid.SelectionMode;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;

@UIScope
@SpringComponent
public class LeftMaintLayout extends VerticalLayout implements ApplicationListener<DispenseProductEvent> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Grid<InventorySlot> grid;
	private ApplicationEventPublisher applicationEventPublisher;

	@Autowired
	public void setApplicationEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
		this.applicationEventPublisher = applicationEventPublisher;
	}

	@Autowired
	InventoryService inventoryService;

	private ComboBox<InventorySlot> inventoryCombo;

	public LeftMaintLayout() {
		setWidth("100%");
		getStyle().set("border", "1px solid #9E9E9E");

		this.grid = new Grid<>(InventorySlot.class);
	}

	@PostConstruct
	public void init() throws Exception {
		grid.setColumns("slotId", "product.name", "product.sellingPrice", "count");

		grid.setSelectionMode(SelectionMode.NONE);

		HorizontalLayout controlPanel = new HorizontalLayout();

		inventoryCombo = buildInventoryComboBox(refreshGrid());

		controlPanel.add(inventoryCombo, buildPlusButton(), buildMinusButton());

		add(grid, controlPanel);

	}

	private List<InventorySlot> refreshGrid() {
		List<InventorySlot> inventory = inventoryService.getAll();

		grid.setItems(inventory);
		return inventory;
	}

	private Button buildPlusButton() {
		Button plusButton = new Button("+");
		plusButton.addClickListener(e -> {
			onPlusClick();

		});

		return plusButton;
	}

	private void onPlusClick() {
		InventorySlot inventorySlot = inventoryCombo.getValue();
		if (inventorySlot != null) {
			try {
				inventoryService.getSlotById(inventorySlot.getSlotId()).addCount();
				applicationEventPublisher.publishEvent(new DispenseProductEvent(this, inventorySlot));
			} catch (ProductSoldOutException | ProductNotFoundException e1) {
				e1.printStackTrace();
			}
		}
	}

	private Button buildMinusButton() {
		Button plusButton = new Button("-");
		plusButton.addClickListener(e -> {
			onMinusClick();
		});

		return plusButton;
	}

	private void onMinusClick() {
		InventorySlot inventorySlot = inventoryCombo.getValue();

		if (inventorySlot != null) {
			try {
				inventoryService.getSlotById(inventorySlot.getSlotId()).dispenseNext();
				applicationEventPublisher.publishEvent(new DispenseProductEvent(this, inventorySlot));
			} catch (ProductSoldOutException | ProductNotFoundException e1) {
				e1.printStackTrace();
			}
		}
	}

	private ComboBox<InventorySlot> buildInventoryComboBox(List<InventorySlot> inventory) {
		ComboBox<InventorySlot> inventoryCombo = new ComboBox<InventorySlot>();

		inventoryCombo.setItemLabelGenerator(g -> g.getSlotId() + " : " + g.getProduct().getName());
		inventoryCombo.setItems(inventory);

		return inventoryCombo;

	}

	@Override
	public void onApplicationEvent(DispenseProductEvent event) {
		grid.setItems(inventoryService.getAll());
	}
}
