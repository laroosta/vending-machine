package com.cyberpro.lrm.vendingmachine.maint.ui;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationListener;

import com.cyberpro.lrm.vendingmachine.pojo.Allocation;
import com.cyberpro.lrm.vendingmachine.pojo.Coin;
import com.cyberpro.lrm.vendingmachine.pojo.Coin.CoinType;
import com.cyberpro.lrm.vendingmachine.pojo.Money;
import com.cyberpro.lrm.vendingmachine.service.AllocationService;
import com.cyberpro.lrm.vendingmachine.service.MoneyBoxService;
import com.cyberpro.lrm.vendingmachine.ui.event.DispenseMoneyEvent;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;

@UIScope
@SpringComponent
public class RightMaintLayout extends VerticalLayout implements ApplicationListener<DispenseMoneyEvent> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Grid<Allocation> grid;
	private List<Money> moneyList;

	@Autowired
	private MoneyBoxService moneyBoxService;

	@Autowired
	private AllocationService allocationService;
	private ComboBox<Money> moneyComboBox;
	private ApplicationEventPublisher applicationEventPublisher;

	@Autowired
	public void setApplicationEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
		this.applicationEventPublisher = applicationEventPublisher;
	}

	public RightMaintLayout() {
		setWidth("100%");
		getStyle().set("border", "1px solid #9E9E9E");

		this.grid = new Grid<>(Allocation.class);
	}

	@PostConstruct
	public void init() throws Exception {
		refreshGrid();

		Label totalLabel = new Label("Total : " + moneyBoxService.getTotal());
		HorizontalLayout controlPanel = new HorizontalLayout();
		moneyComboBox = buildMoneyComboBox();
		controlPanel.add(moneyComboBox, buildPlusButton(), buildMinusButton());

		add(grid, totalLabel, controlPanel);
	}

	private ComboBox<Money> buildMoneyComboBox() {
		moneyComboBox = new ComboBox<Money>();
		moneyComboBox.setItemLabelGenerator(m -> "" + m.getValue());

		List<Money> coinList = new ArrayList<>();
		coinList.add(new Coin(CoinType.FIFTYCENTS));
		coinList.add(new Coin(CoinType.ONE));
		coinList.add(new Coin(CoinType.TWO));
		coinList.add(new Coin(CoinType.FIVE));
		moneyComboBox.setItems(coinList);

		return moneyComboBox;
	}

	private Button buildPlusButton() {
		Button plusButton = new Button("+");
		plusButton.addClickListener(e -> {
			onPlusClick();

		});

		return plusButton;
	}

	private void onPlusClick() {
		Money money = moneyComboBox.getValue();
		if (money != null) {
			moneyBoxService.deposit(money);
			applicationEventPublisher.publishEvent(new DispenseMoneyEvent(this, null));
		}
	}

	private Button buildMinusButton() {
		Button plusButton = new Button("-");
		plusButton.addClickListener(e -> {
			onMinusClick();
		});

		return plusButton;
	}

	private void onMinusClick() {
		Money money = moneyComboBox.getValue();
		if (money != null) {
			moneyBoxService.withdraw(money.getValue());
			applicationEventPublisher.publishEvent(new DispenseMoneyEvent(this, null));
		}
	}

	@Override
	public void onApplicationEvent(DispenseMoneyEvent event) {
		refreshGrid();
	}

	private void refreshGrid() {
		moneyList = moneyBoxService.getMoneyList();
		grid.setItems(allocationService.getAllocation(moneyList));
	}

}
