package com.cyberpro.lrm.vendingmachine.maint.ui;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.router.Route;

@Route("maint")
public class MaintenanceView extends HorizontalLayout {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Autowired
	RightMaintLayout rightLayout;

	@Autowired
	LeftMaintLayout leftLayout;

	@PostConstruct
	public void init() throws Exception {
		add(leftLayout, rightLayout);
	}

}
