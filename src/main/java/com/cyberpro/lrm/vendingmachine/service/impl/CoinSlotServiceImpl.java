package com.cyberpro.lrm.vendingmachine.service.impl;

import org.springframework.stereotype.Component;

/**
 * @author lmichelson
 *
 */
@Component(value = "coinSlot")
public class CoinSlotServiceImpl extends AbstractMoneySlotService {

}
