package com.cyberpro.lrm.vendingmachine.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.cyberpro.lrm.vendingmachine.pojo.Allocation;
import com.cyberpro.lrm.vendingmachine.pojo.Money;

/**
 * Keeps track of money that is deposited or withdrawn
 * 
 * @author lmichelson
 *
 */
@Service
public interface MoneyBoxService {
	/**
	 * Used to deposit Money into the cash box
	 * 
	 * @param money
	 */
	void deposit(Money money);

	/**
	 * Used to deposit List of Money into the cash box
	 * 
	 * @param money
	 */
	void deposit(List<Money> money);

	/**
	 * @return
	 */
	double getTotal();

	/**
	 * @return list of available money
	 */
	List<Money> getMoneyList();

	/**
	 * @param allocation
	 * @return
	 */
	void withdraw(List<Allocation> allocation);

	/**
	 * @param money
	 */
	void withdraw(double value);

}
