package com.cyberpro.lrm.vendingmachine.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.cyberpro.lrm.vendingmachine.pojo.Money;

/**
 * This class is used to maintain a list of Money inserted during a transaction.
 * 
 * @author lmichelson
 *
 */
@Service
public interface MoneySlotService {
	/**
	 * Used to insert money into the slot for the current transaction.
	 * 
	 * @param money
	 */
	void insert(Money money);

	/**
	 * Gets a List of Money inserted into the slot during the transaction.
	 * 
	 * @return
	 */
	List<Money> getAll();

	/**
	 * Clears all the money from this slot and concludes this transaction.
	 * 
	 */
	void clear();

	/**
	 * Get the total of money in the slot for the current transaction.
	 * 
	 * @return
	 */
	double getTotal();
}
