package com.cyberpro.lrm.vendingmachine.service.impl;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import com.cyberpro.lrm.vendingmachine.pojo.Allocation;
import com.cyberpro.lrm.vendingmachine.pojo.Money;
import com.cyberpro.lrm.vendingmachine.service.AllocationService;

/**
 * This class is used to convert a list of Money and return the allocation
 * thereof.
 * 
 * @author lmichelson
 *
 */
@Component
public class AllocationServiceImpl implements AllocationService {

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cyberpro.lrm.vendingmachine.service.AllocationService#getAll()
	 */
	@Override
	public List<Allocation> getAllocation(List<Money> moneyList) {
		return convertToList(aggregateMoneyList(moneyList));
	}

	/**
	 * Converts the disaggregated money list to a map
	 * 
	 * @param groupMap
	 * @return
	 */
	private List<Allocation> convertToList(Map<Double, Long> groupMap) {
		return groupMap.entrySet().stream().map(m -> new Allocation(m.getKey(), Math.toIntExact(m.getValue())))
				.collect(Collectors.toList());
	}

	/**
	 * Groups the money by value and count
	 * 
	 * @param moneyList
	 * @return
	 */
	private Map<Double, Long> aggregateMoneyList(List<Money> moneyList) {
		return moneyList.stream().collect(Collectors.groupingBy(Money::getValue, Collectors.counting()));
	}

}
