package com.cyberpro.lrm.vendingmachine.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.cyberpro.lrm.vendingmachine.pojo.Allocation;
import com.cyberpro.lrm.vendingmachine.pojo.Money;
import com.cyberpro.lrm.vendingmachine.service.MoneyBoxService;

/**
 * @author lmichelson
 *
 */
@Component
public class MoneyBoxServiceImpl implements MoneyBoxService {

	private static final Logger log = LoggerFactory.getLogger(MoneyBoxService.class);

	private List<Money> moneyList = new ArrayList<Money>();

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cyberpro.lrm.vendingmachine.service.MoneyBoxService#add(com.cyberpro.lrm.
	 * vendingmachine.pojo.Money)
	 */
	@Override
	public void deposit(Money money) {
		log.info("Money deposited into MoneyBox: " + money.getValue());
		moneyList.add(money);
	}

	@Override
	public void deposit(List<Money> depositMoneyList) {
		log.info("Money deposited into MoneyBox: " + getTotalFromList(depositMoneyList));
		moneyList.addAll(depositMoneyList);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cyberpro.lrm.vendingmachine.service.MoneyBoxService#getTotal()
	 */
	@Override
	public double getTotal() {
		return getTotalFromList(moneyList);
	}

	private double getTotalFromList(List<Money> moneyList) {
		return moneyList.stream().mapToDouble(m -> m.getValue()).sum();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cyberpro.lrm.vendingmachine.service.MoneyBoxService#getMoneyList()
	 */
	@Override
	public List<Money> getMoneyList() {
		return moneyList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cyberpro.lrm.vendingmachine.service.MoneyBoxService#withdraw(java.util.
	 * List)
	 */
	@Override
	public void withdraw(List<Allocation> allocations) {
		for (Allocation allocation : allocations) {
			int count = allocation.getCount();

			while (count > 0) {
				withdraw(allocation.getValue());
				count--;
			}
		}

		log.info("Money remaining in MoneyBox: " + getTotalFromList(moneyList));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cyberpro.lrm.vendingmachine.service.MoneyBoxService#withdraw(com.cyberpro
	 * .lrm.vendingmachine.pojo.Money)
	 */
	@Override
	public void withdraw(double value) {
		if (getTotal() != 0) {
			log.info("Money widthdrawing from MoneyBox: " + value);
			Money money = moneyList.stream().filter(m -> m.getValue() == value).findFirst().get();
			moneyList.remove(money);
		}
	}

}
