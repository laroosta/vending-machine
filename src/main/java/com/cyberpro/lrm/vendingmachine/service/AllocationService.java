package com.cyberpro.lrm.vendingmachine.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.cyberpro.lrm.vendingmachine.pojo.Allocation;
import com.cyberpro.lrm.vendingmachine.pojo.Money;

/**
 * The AllocationService is used to convert a list of Money and return the
 * allocation thereof.
 * 
 * @author lmichelson
 *
 */
@Service
public interface AllocationService {

	/**
	 * @return
	 */
	public List<Allocation> getAllocation(List<Money> moneyList);

}
