package com.cyberpro.lrm.vendingmachine.service.impl;

import org.springframework.stereotype.Component;

/**
 * @author lmichelson
 *
 */
@Component(value = "bankNoteSlot")
public class BankNoteSlotServiceImpl extends AbstractMoneySlotService {

}
