package com.cyberpro.lrm.vendingmachine.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.cyberpro.lrm.vendingmachine.pojo.Money;
import com.cyberpro.lrm.vendingmachine.service.MoneySlotService;

/**
 * This is the Abstract class to manage all the different methods of depositing
 * money. It sets the baseline which then can easily be extended with specific
 * functionality, such as validating etc. I added a few other examples:
 * 
 * @see CoinSlotServiceImpl
 * @see BankNoteSlotServiceImpl
 * @see CardSlotServiceImpl
 * 
 * @author lmichelson
 *
 */
public abstract class AbstractMoneySlotService implements MoneySlotService {

	private List<Money> moneyList = new ArrayList<>();
	private double totalValue = 0;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cyberpro.lrm.vendingmachine.service.MoneySlotService#insert(com.cyberpro.
	 * lrm.vendingmachine.pojo.Money)
	 */
	@Override
	public void insert(Money money) {
		moneyList.add(money);
		totalValue += money.getValue();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cyberpro.lrm.vendingmachine.service.MoneySlotService#getAll()
	 */
	@Override
	public List<Money> getAll() {
		return moneyList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cyberpro.lrm.vendingmachine.service.MoneySlotService#clear()
	 */
	@Override
	public void clear() {
		moneyList.clear();
		totalValue = 0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cyberpro.lrm.vendingmachine.service.MoneySlotService#getTotal()
	 */
	@Override
	public double getTotal() {
		return totalValue;
	}
}
