package com.cyberpro.lrm.vendingmachine.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.cyberpro.lrm.vendingmachine.exception.NotEnoughChangeException;
import com.cyberpro.lrm.vendingmachine.pojo.Allocation;

/**
 * The MoneyDispenserService calculates and returns denominations based on the
 * money allocation and availability
 * 
 * @author lmichelson
 *
 */
@Service
public interface MoneyDispenserService {

	/**
	 * Expects the allocation of money and the amount to dispense, then calculates
	 * how many of each type to dispense
	 * 
	 * @param allocation
	 * @param amount
	 * @return
	 * @throws NotEnoughChangeException
	 */
	public List<Allocation> calculateDenominations(List<Allocation> allocation, double amount) throws NotEnoughChangeException;

}
