package com.cyberpro.lrm.vendingmachine.service;

import java.util.List;

import com.cyberpro.lrm.vendingmachine.exception.ProductNotFoundException;
import com.cyberpro.lrm.vendingmachine.exception.ProductSoldOutException;
import com.cyberpro.lrm.vendingmachine.pojo.InventorySlot;

/**
 * This class manages the inventory through a InventorySlot
 * 
 * @author lmichelson
 *
 */
public interface InventoryService {

	/**
	 * Adds an InventorySlot which contains the slotId, Product and inventory count.
	 * 
	 * @param slot
	 */
	void addSlot(InventorySlot slot);

	/**
	 * Returns an InventorySlot
	 * 
	 * @param string which the is slotID
	 * @return
	 * @throws ProductSoldOutException  when the InventorySlot count is 0
	 * @throws ProductNotFoundException when the wrong slot has been selected.
	 */
	InventorySlot getSlotById(String string) throws ProductSoldOutException, ProductNotFoundException;

	/**
	 * Returns a List of InventorySlot
	 * 
	 * @return
	 */
	List<InventorySlot> getAll();
}
