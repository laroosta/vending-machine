package com.cyberpro.lrm.vendingmachine.service.impl;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.cyberpro.lrm.vendingmachine.exception.NotEnoughChangeException;
import com.cyberpro.lrm.vendingmachine.pojo.Allocation;
import com.cyberpro.lrm.vendingmachine.service.MoneyDispenserService;

/**
 * @author lmichelson
 *
 */
@Component
public class MoneyDispenserServiceImpl implements MoneyDispenserService {

	private static final Logger log = LoggerFactory.getLogger(MoneyDispenserService.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cyberpro.lrm.vendingmachine.service.MoneyDispenserService#
	 * calculateDenominations(java.util.Map, double)
	 */
	@Override
	public List<Allocation> calculateDenominations(List<Allocation> allocations, double amount)
			throws NotEnoughChangeException {
		log.info("Calcating denominations for amount : " + amount);
		Map<Double, Integer> denomination = new HashMap<Double, Integer>();

		if (amount == 0.00) {
			log.info("Amount was 0.00, returning empty denomination");
			return convertToListOfAllocation(denomination);
		}

		List<Allocation> sortedAllocations = sortAllocationsDescending(allocations);

		double remainingAmount = amount;
		double availableAmount = getTotalAllocation(allocations);
		log.info("Total Amount Available : " + availableAmount);

		if (remainingAmount > availableAmount) {
			log.info("Not enough change available ");
			throw new NotEnoughChangeException();
		}

		for (Allocation allocation : sortedAllocations) {
			if (remainingAmount == 0.00) {
				log.info("Remaining amount 0.00, ending denominations");
				break;
			}

			if (allocation.getValue() > amount) {
				continue;
			}

			int depenseCount = (int) Math.abs(remainingAmount / allocation.getValue());
			int allocationCount = allocation.getCount();

			if (depenseCount > allocationCount) {
				log.info("depenseCount greater than allocationCount. Can only dispense : " + allocationCount);
				depenseCount = allocationCount;
			}

			if (hasSufficientForDispensing(allocationCount, depenseCount)) {
				remainingAmount -= depenseCount * allocation.getValue();
				log.info("Adding money value to denomination : " + allocation.getValue() + " = " + depenseCount
						+ " - Remaining Amount : " + remainingAmount);
				denomination.put(allocation.getValue(), depenseCount);
			}

		}

		if (remainingAmount > 0) {
			throw new NotEnoughChangeException();
		}

		return convertToListOfAllocation(denomination);
	}

	private List<Allocation> convertToListOfAllocation(Map<Double, Integer> denomination) {
		return denomination.entrySet().stream().map(m -> new Allocation(m.getKey(), m.getValue()))
				.collect(Collectors.toList());
	}

	private boolean hasSufficientForDispensing(int allocatedCount, int depenseCount) {
		return depenseCount > 0 && depenseCount <= allocatedCount;
	}

	/**
	 * Sorts the allocations descending
	 * 
	 * @param allocations
	 * @return
	 */
	private List<Allocation> sortAllocationsDescending(List<Allocation> allocations) {
		List<Allocation> sortedAllocations = allocations.stream()
				.sorted(Comparator.comparing(Allocation::getValue, (o1, o2) -> o1.compareTo(o2)).reversed())
				.collect(Collectors.toList());
		return sortedAllocations;
	}

	/**
	 * Calculates the total value of the allocation list and returns
	 * 
	 * @param allocation
	 * @return
	 */
	private double getTotalAllocation(List<Allocation> allocation) {
		return allocation.stream().mapToDouble(m -> m.getValue() * m.getCount()).sum();
	}

}
