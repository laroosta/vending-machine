package com.cyberpro.lrm.vendingmachine.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.cyberpro.lrm.vendingmachine.exception.ProductNotFoundException;
import com.cyberpro.lrm.vendingmachine.exception.ProductSoldOutException;
import com.cyberpro.lrm.vendingmachine.pojo.InventorySlot;
import com.cyberpro.lrm.vendingmachine.service.InventoryService;

@Component
public class InventoryServiceImpl implements InventoryService {

	private static final Logger log = LoggerFactory.getLogger(InventoryService.class);

	List<InventorySlot> slotList = new ArrayList<>();

	public InventoryServiceImpl() {
		super();
	}

	@Override
	public void addSlot(InventorySlot slot) {
		log.info("Adding new InventorySlot : " + slot.getSlotId() + " - Product : " + slot.getProduct().getName());
		slotList.add(slot);
	}

	@Override
	public InventorySlot getSlotById(String slotId) throws ProductSoldOutException, ProductNotFoundException {
		InventorySlot inventorySlot = slotList.stream().filter(s -> slotId.compareTo(s.getSlotId()) == 0).findFirst()
				.get();

		if (inventorySlot == null) {
			throw new ProductNotFoundException();
		}

		return inventorySlot;
	}

	@Override
	public List<InventorySlot> getAll() {
		return slotList;
	}

}
