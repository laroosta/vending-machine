package com.cyberpro.lrm.vendingmachine.controller;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import com.cyberpro.lrm.vendingmachine.exception.InsufficientFundsException;
import com.cyberpro.lrm.vendingmachine.exception.NotEnoughChangeException;
import com.cyberpro.lrm.vendingmachine.exception.ProductNotFoundException;
import com.cyberpro.lrm.vendingmachine.exception.ProductSoldOutException;
import com.cyberpro.lrm.vendingmachine.pojo.Allocation;
import com.cyberpro.lrm.vendingmachine.pojo.Coin;
import com.cyberpro.lrm.vendingmachine.pojo.Coin.CoinType;
import com.cyberpro.lrm.vendingmachine.pojo.InventorySlot;
import com.cyberpro.lrm.vendingmachine.pojo.Money;
import com.cyberpro.lrm.vendingmachine.pojo.Product;
import com.cyberpro.lrm.vendingmachine.service.AllocationService;
import com.cyberpro.lrm.vendingmachine.service.InventoryService;
import com.cyberpro.lrm.vendingmachine.service.MoneyBoxService;
import com.cyberpro.lrm.vendingmachine.service.MoneyDispenserService;
import com.cyberpro.lrm.vendingmachine.service.MoneySlotService;
import com.cyberpro.lrm.vendingmachine.ui.event.DispenseMoneyEvent;
import com.cyberpro.lrm.vendingmachine.ui.event.DispenseProductEvent;
import com.cyberpro.lrm.vendingmachine.ui.event.DisplayChangeEvent;
import com.cyberpro.lrm.vendingmachine.ui.event.SystemEvent;

/**
 * The VendingMachineController class is where the entire application comes
 * together. It instantiates majority of the services and initializes all the
 * data. The controller implements an ApplicationListener<SystemEvent>, which
 * listens to events throughout the application and performs an action based on
 * those events using the onApplicationEvent() method.
 * 
 * @author lmichelson
 *
 */
@Component
public class VendingMachineController implements ApplicationListener<SystemEvent> {

	private static final Logger log = LoggerFactory.getLogger(VendingMachineController.class);

	private ApplicationEventPublisher applicationEventPublisher;
	private MoneySlotService coinSlotService;
	private MoneyBoxService moneyBoxService;
	private MoneyDispenserService moneyDispenserService;
	private AllocationService allocationService;
	private InventoryService inventoryService;
	// private VendingClusterService vendingClusterBootstrap;

	@PostConstruct
	public void init() {
		initMoneyBox();
		initInventory();
		// vendingClusterBootstrap.joinCluster();
	}

//	@Autowired
//	public void setVendingClusterBootstrap(VendingClusterService vendingClusterBootstrap) {
//		this.vendingClusterBootstrap = vendingClusterBootstrap;
//	}

	@Autowired
	public void setInventoryService(InventoryService inventoryService) {
		this.inventoryService = inventoryService;
	}

	@Autowired
	@Qualifier("coinSlot")
	public void setMoneySlotService(MoneySlotService coinSlotService) {
		this.coinSlotService = coinSlotService;
	}

	@Autowired
	public void setMoneyBoxService(MoneyBoxService moneyBoxService) {
		this.moneyBoxService = moneyBoxService;
	}

	@Autowired
	public void setMoneyDispenserService(MoneyDispenserService moneyDispenserService) {
		this.moneyDispenserService = moneyDispenserService;
	}

	@Autowired
	public void setAllocationService(AllocationService allocationService) {
		this.allocationService = allocationService;
	}

	@Autowired
	public void setApplicationEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
		this.applicationEventPublisher = applicationEventPublisher;
	}

	/**
	 * Initializes the Inventory with dummy data. This would typically use a data
	 * service to retrieve or store the data in either a file format or a database.
	 */
	private void initInventory() {
		InventorySlot inventorySlotCoke = new InventorySlot("AA", new Product("123", "Coke", 10.00), 5);
		inventoryService.addSlot(inventorySlotCoke);

		InventorySlot inventorySlotCokeLite = new InventorySlot("AB", new Product("124", "Coke Lite", 11.00), 5);
		inventoryService.addSlot(inventorySlotCokeLite);

		InventorySlot inventorySlotFanta = new InventorySlot("BA", new Product("321", "Fanta", 12.50), 5);
		inventoryService.addSlot(inventorySlotFanta);

		InventorySlot inventorySlotCreamSoda = new InventorySlot("BB", new Product("322", "CreamSoda", 15.00), 5);
		inventoryService.addSlot(inventorySlotCreamSoda);
	}

	/**
	 * Initializes the MoneyBox with dummy data. This would typically use a data
	 * service to retrieve or store the data in either a file format or a database.
	 */
	private void initMoneyBox() {
		List<Money> moneyList = new ArrayList<>();

		for (CoinType coinType : CoinType.values()) {
			for (int i = 0; i < 10; i++) {
				moneyList.add(new Coin(coinType));
			}
		}

		moneyBoxService.deposit(moneyList);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.context.ApplicationListener#onApplicationEvent(org.
	 * springframework.context.ApplicationEvent)
	 */
	@Override
	public void onApplicationEvent(SystemEvent event) {

		switch (event.getType()) {
		case INSERT_MONEY:
			handleInsertMoney(event);
			break;

		case DISPENSE_ITEM:
			handleDispenseItem(event);
			break;

		case RETURN_MONEY:
			handleReturnMoney();
			break;

		case CLEAR_INPUT:
			DisplayChangeEvent displayChangeEvent = new DisplayChangeEvent(this, "Please Make Selection", null);
			applicationEventPublisher.publishEvent(displayChangeEvent);
			break;

		default:
			resetDisplay();
			break;

		}
	}

	private void handleReturnMoney() {
		log.info("Returning Money");
		double totalAmount = coinSlotService.getTotal();

		try {
			dispenseMoney(totalAmount);
			resetDisplay();
		} catch (NotEnoughChangeException e) {
			DisplayChangeEvent displayChangeEvent = new DisplayChangeEvent(this,
					"Unable to dispense your change, please call support", "011 555 1234");
			applicationEventPublisher.publishEvent(displayChangeEvent);
		}
	}

	private void resetDisplay() {
		DisplayChangeEvent displayChangeEvent = new DisplayChangeEvent(this, "Please Insert Coins", "Total : 0.00");
		applicationEventPublisher.publishEvent(displayChangeEvent);
	}

	private void handleDispenseItem(SystemEvent event) {
		String message = handleSelection(event);

		DisplayChangeEvent displayChangeEvent = new DisplayChangeEvent(this, message,
				"Total : " + coinSlotService.getTotal());
		applicationEventPublisher.publishEvent(displayChangeEvent);
	}

	private String handleSelection(SystemEvent event) {
		double depositedAmount = coinSlotService.getTotal();
		String entry = (String) event.getObject()[0];

		String message;

		try {
			InventorySlot inventory = inventoryService.getSlotById(entry);
			double sellingPrice = inventory.getProduct().getSellingPrice();

			if (sellingPrice > depositedAmount) {
				throw new InsufficientFundsException();
			}
			dispenseFromInventory(inventory);

			double changeAmount = depositedAmount - sellingPrice;
			dispenseMoney(changeAmount);

			message = "Change : " + changeAmount;
		} catch (ProductSoldOutException e) {
			message = "Item Sold out, select another";
		} catch (ProductNotFoundException e) {
			message = "Selection not found, select another";
		} catch (InsufficientFundsException e) {
			message = "Insufficient Funds";
		} catch (NotEnoughChangeException e) {
			message = "No Change";
		}
		return message;
	}

	private void dispenseMoney(double changeAmount) throws NotEnoughChangeException {
		List<Allocation> allocation = getAllocationFromMoneyBox();
		List<Allocation> changeDenomination = moneyDispenserService.calculateDenominations(allocation, changeAmount);

		moneyBoxService.withdraw(changeDenomination);

		applicationEventPublisher.publishEvent(new DispenseMoneyEvent(this, changeDenomination));
		coinSlotService.clear();
	}

	private void dispenseFromInventory(InventorySlot inventory) throws ProductSoldOutException {
		inventory.dispenseNext();
		applicationEventPublisher.publishEvent(new DispenseProductEvent(this, inventory));
	}

	private List<Allocation> getAllocationFromMoneyBox() {
		List<Money> moneyList = moneyBoxService.getMoneyList();
		List<Allocation> allocation = allocationService.getAllocation(moneyList);
		return allocation;
	}

	private void handleInsertMoney(SystemEvent event) {
		DisplayChangeEvent displayChangeEvent = new DisplayChangeEvent(this, event.getMessage(),
				"Total : " + coinSlotService.getTotal());
		applicationEventPublisher.publishEvent(displayChangeEvent);
	}

}
